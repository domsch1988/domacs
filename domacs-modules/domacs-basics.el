(use-package emacs
  :ensure nil
  :config
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (recentf-mode 1)
  (add-to-list 'exec-path "/usr/local/lib/nodejs/node-v20.11.0-linux-x64/bin")
  (add-to-list 'exec-path "/home/dosa/.local/bin")
  (define-prefix-command 'domacs-map)
  (global-set-key (kbd "C-d") 'domacs-map)
  :custom
  (visible-bell t)
  (tab-always-indent 'complete)
  (inhibit-startup-message t)
  ;; :hook
  ;; (after-init . toggle-frame-maximized)
  ;; :custom-face
  ;; (default ((t (:font ,dom/font-fixed :height ,dom/size-fixed))))
  ;; (fixed-pitch ((t (:family ,dom/font-fixed :height ,dom/size-fixed))))
  ;; (variable-pitch ((t (:family ,dom/font-variable :height ,dom/size-variable))))
  :bind
  (
   ("<escape>" . keyboard-quit)
   ("C-c w 1" . domacs--wm-setup-one)
   :map domacs-map
   ("f" . domacs--indent-buffer)
   ("e d" . eval-defun)
   )
  )

(use-package comp
  :ensure nil
  :custom
  (native-comp-async-report-warnings-errors nil)
  )

(use-package package
  :ensure nil
  :custom
  (package-install-upgrade-built-in t)
  )

(use-package files
  :ensure nil
  :config
  (defun domacs--auto-tangle ()
    "This Function is used to Automatically tangle org files on Save"
    (when (eq major-mode 'org-mode) (org-babel-tangle)))
  (add-hook 'after-save-hook #'domacs--auto-tangle)
  :custom
  (confirm-kill-processes nil)
  (make-backup-files nil)
  (auto-save-file-name-transforms
   `((".*" ,temporary-file-directory t)))
  (backup-directory-alist
   `((".*" . ,temporary-file-directory)))
  (major-mode-remap-alist
   `((json-mode . json-ts-mode)
     (js-json-mode . json-ts-mode)
     ))
  )

(use-package savehist
  :ensure nil
  :init
  (savehist-mode))

(use-package autorevert :ensure nil :delight)

(provide 'domacs-basics)
