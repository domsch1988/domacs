(use-package magit
  :config

  :bind
  (
   ("C-c g m" . magit)
   ("C-c g l" . magit-log-buffer-file)
   ))

(use-package treemacs-magit)

(use-package transient)

(use-package keychain-environment
  :init
  (keychain-refresh-environment)
  )

(use-package blamer
  :bind (("C-c g b" . blamer-show-commit-info))
  :defer 5
  :custom
  (blamer-idle-time 0.3)
  (blamer-min-offset 70)
  (blamer-enable-async-execution-p nil)
  :custom-face
  (blamer-face ((t 
		 :background unspecified
		 :height 100
		 :italic t)))
  )

(use-package git-gutter
  :config
  (global-git-gutter-mode t)
)

(provide 'domacs-git)
