(use-package dired
  :ensure nil
  :custom
  (dired-auto-revert-buffer #'dired-directory-changed-p)
  (dired-make-directory-clickable t)
  (dired-free-space nil)
  (dired-mouse-drag-files t)
  (dired-listing-switches
   "-AGFhlv --group-directories-first --time-style=long-iso")
  :hook
  (dired-mode . dired-hide-details-mode)
  (dired-mode . hl-line-mode)
  )

(use-package treemacs
  :ensure t
  :after (modus-themes)
  :config
  (defun domacs--customize-treemacs ()
    (custom-set-faces
     `(treemacs-window-background-face ((t (:background ,(modus-themes-get-color-value 'bg-dim)))))
     `(treemacs-root-face ((t (:family ,dom/font-slab :height 180 :underline nil :foreground ,(modus-themes-get-color-value 'fg-heading-0))))) 
     `(treemacs-file-face ((t (:family ,dom/font-variable :height ,dom/size-variable :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-directory-face ((t (:family ,dom/font-variable :height ,dom/size-variable :foreground ,(modus-themes-get-color-value 'fg-main) :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-hl-line-face ((t (:inherit hl-line :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-hl-line))))))
     `(treemacs-git-untracked-face ((t (:family ,dom/font-variable :height ,dom/size-variable :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-git-added-face ((t (:family ,dom/font-variable :height ,dom/size-variable :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-git-modified-face ((t (:family ,dom/font-variable :height ,dom/size-variable :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-git-commit-diff-face ((t (:family ,dom/font-variable :height ,dom/size-variable :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim)))))) 
     `(treemacs-git-ignored-face ((t (:family ,dom/font-variable :height ,dom/size-variable :foreground ,(modus-themes-get-color-value 'fg-dim) :box (:line-width (1 . 2) :color ,(modus-themes-get-color-value 'bg-dim))))))
     )
    )
  (domacs--customize-treemacs)
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
  	  treemacs-deferred-git-apply-delay        0.5
  	  treemacs-directory-name-transformer      #'identity
  	  treemacs-display-in-side-window          t
  	  treemacs-eldoc-display                   'simple
  	  treemacs-file-event-delay                2000
  	  treemacs-file-extension-regex            treemacs-last-period-regex-value
  	  treemacs-file-follow-delay               0.2
  	  treemacs-file-name-transformer           #'identity
  	  treemacs-follow-after-init               t
  	  treemacs-expand-after-init               t
  	  treemacs-find-workspace-method           'find-for-file-or-pick-first
  	  treemacs-git-command-pipe                ""
  	  treemacs-goto-tag-strategy               'refetch-index
  	  treemacs-header-scroll-indicators        '(nil . "^^^^^^")
  	  treemacs-hide-dot-git-directory          t
  	  treemacs-indentation                     2
  	  treemacs-indentation-string              " "
  	  treemacs-is-never-other-window           nil
  	  treemacs-max-git-entries                 5000
  	  treemacs-missing-project-action          'ask
  	  treemacs-move-forward-on-expand          nil
  	  treemacs-no-png-images                   nil
  	  treemacs-no-delete-other-windows         t
  	  treemacs-project-follow-cleanup          t
  	  treemacs-project-follow-mode             t
  	  treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
  	  treemacs-position                        'left
  	  treemacs-read-string-input               'from-child-frame
  	  treemacs-recenter-distance               0.1
  	  treemacs-recenter-after-file-follow      nil
  	  treemacs-recenter-after-tag-follow       nil
  	  treemacs-recenter-after-project-jump     'always
  	  treemacs-recenter-after-project-expand   'on-distance
  	  treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
  	  treemacs-project-follow-into-home        nil
  	  treemacs-show-cursor                     nil
  	  treemacs-show-hidden-files               t
  	  treemacs-silent-filewatch                nil
  	  treemacs-silent-refresh                  nil
  	  treemacs-sorting                         'alphabetic-asc
  	  treemacs-select-when-already-in-treemacs 'move-back
  	  treemacs-space-between-root-nodes        t
  	  treemacs-tag-follow-cleanup              t
  	  treemacs-tag-follow-delay                1.5
  	  treemacs-text-scale                      nil
  	  treemacs-user-mode-line-format           'none
  	  treemacs-user-header-line-format         nil
  	  treemacs-wide-toggle-width               70
  	  treemacs-width                           35
  	  treemacs-width-increment                 1
  	  treemacs-width-is-initially-locked       nil
  	  treemacs-workspace-switch-cleanup        nil)
    (treemacs-resize-icons 16)
    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
  		 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :hook
  (modus-themes-post-load . domacs--customize-treemacs)
  (ef-themes-post-load . domacs--customize-treemacs)
  ;; (emacs-startup . treemacs)
  :bind
  (:map global-map
  	("C-x t t"   . treemacs)))

(use-package treemacs-all-the-icons
  :requires all-the-icons)

(use-package treemacs-nerd-icons
  :requires nerd-icons
  :config
  (treemacs-load-theme 'nerd-icons))
(provide 'domacs-files)
