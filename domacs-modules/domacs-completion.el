(use-package which-key
  :delight
  :config (which-key-mode 1))

(use-package vertico
  :init
  (vertico-mode)
  :custom
  (vertico-count 20)
  (vertico-resize nil)
  )

(use-package orderless
  :config
  (setq completion-styles '(orderless partial-completion basic)
	completion-category-defaults nil
	completion-category-overrides '((eglot (styles orderless))
					(eglot-capf (styles orderless))))
  )

(use-package consult
  ;; :bind (;; C-c bindings in `mode-specific-map'
  ;; 	 ("C-c M-x" . consult-mode-command)
  ;; 	 ([remap Info-search] . consult-info)
  ;; 	 ;; C-x bindings in `ctl-x-map'
  ;; 	 ("<S-S> <S-S>" . consult-buffer)                ;; orig. switch-to-buffer
  ;; 	 ("C-x w b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
  ;; 	 ("C-x t b" . consult-buffer-other-tab)    ;; orig. switch-to-buffer-other-tab
  ;; 	 ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
  ;; 	 ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
  ;; 	 ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
  ;; 	 ("M-g i" . consult-imenu)
  ;; 	 ("M-g I" . consult-imenu-multi)
  ;; 	 ;; M-s bindings in `search-map'

  ;; 	 ("M-s d" . consult-find)                  ;; Alternative: consult-fd
  ;; 	 ("M-s c" . consult-locate)
  ;; 	 ("M-s g" . consult-grep)
  ;; 	 ("M-s G" . consult-git-grep)
  ;; 	 ("M-s r" . consult-ripgrep)
  ;; 	 ("M-s l" . consult-line)
  ;; 	 ("M-s L" . consult-line-multi)
  ;; 	 ("M-s k" . consult-keep-lines)
  ;; 	 ("M-s u" . consult-focus-lines)
  ;; 	 :map minibuffer-local-map
  ;; 	 ("M-s" . consult-history)                 ;; orig. next-matching-history-element
  ;; 	 ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
	register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
	xref-show-definitions-function #'consult-xref)
  :config
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))
  )

(use-package marginalia
  :bind (:map minibuffer-local-map
	      ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(use-package corfu
  ;; Optional customizations
  :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  :bind (:map corfu-map
  	      ("<return>" . newline)
  	      ("C-<return>" . corfu-insert)
  	      )
  :init
  (global-corfu-mode))

(use-package cape
  :config
  (defun domacs--eglot-capf ()
    (setq-local completion-at-point-functions
		(list (cape-capf-super
                       #'yasnippet-capf
		       #'cape-file
		       #'cape-dabbrev
		       #'cape-keyword))))
  (defun domacs--set-capf ()
    (interactive)
    #'domacs--eglot-capf)

  (add-hook 'eglot-managed-mode-hook #'domacs--eglot-capf)
  (add-hook 'find-file-hook #'domacs--eglot-capf)
  (add-hook 'org-src-mode-hook #'domacs--eglot-capf)
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-to-list 'completion-at-point-functions #'yasnippet-capf)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-file)
  ;; (add-to-list 'completion-at-point-functions #'cape-elisp-block)
  ;;(add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-dict)
  ;;(add-to-list 'completion-at-point-functions #'cape-elisp-symbol)
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  )

(use-package yasnippet-capf)

(use-package yasnippet
  :delight
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :disabled)

(provide 'domacs-completion)
