(use-package mu4e
  :ensure nil
  :after (org)
  :custom
  (mu4e-sent-folder "/Sent")
  (mu4e-drafts-folder "/Entw&APw-rfe")
  (mu4e-trash-folder "/Papierkorb")
  (mu4e-get-mail-command "offlineimap")
  )

(use-package gnus
  :ensure nil
  :config
  (setq
   user-mail-address "dominik@schlack.net"
   user-full-name "Dominik Schlack"
   smtpmail-smtp-server "smtp.strato.de"
   send-mail-function 'smtpmail-send-it
   smtpmail-smtp-service 587
   gnus-select-method '(nnimap "imap.strato.de"))
  )

(provide 'domacs-mail)
