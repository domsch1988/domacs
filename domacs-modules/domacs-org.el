(use-package org
  :defer t
  :config
  (cond
   ((memq window-system '(w32))
    (setq org-capture-templates
	  '(("t" "Todo" entry (file+headline "d:/03_Documents/02_Notes/task.org" "Tasks")
	     "* TODO %? :%(\n  %i")
	    ("n" "Quicknote" entry (file+headline "d:/03_Documents/02_Notes/notes.org" "Notes")
	     "* %?\n  %i\n  %a")
	    ("j" "Journal" entry (file+datetree  "d:/03_Documents/02_Notes/journal.org" "Journal")
	     "* %?\nEntered on %U\n  %i\n  %a")
	    ("b" "Bookmark" entry (file+datetree  "d:/03_Documents/02_Notes/bookmarks.org" "Bookmark")
	     "*** %? %^G\n[[%x]]\n- "))
	  )
    )
   ((memq system-type '(gnu/linux))
    (setq org-capture-templates
	  '(("t" "Todo" entry (file+headline "~/Daten/03_Documents/02_Notes/tasks.org" "Tasks")
	     "* TODO %?\n  %i")
	    ("n" "Quicknote" entry (file+headline "~/Daten/03_Documents/02_Notes/notes.org" "Notes")
	     "* %?\n  %i\n  %a")
	    ("j" "Journal" entry (file+datetree  "~/Daten/03_Documents/02_Notes/journal.org" "Journal")
	     "* %?\nEntered on %U\n  %i\n  %a")
	    ("b" "Bookmark" entry (file+headline "~/Daten/03_Documents/02_Notes/bookmarks.org" "Emacs")
	     "\n\n*** %? %^G\n[[%c]]\n- "))
	  )
    )
   )
  (add-to-list 'org-structure-template-alist '("t" . "src elisp :tangle"))
  (add-to-list 'org-structure-template-alist '("ti" . "src elisp :tangle init.el"))
  (add-to-list 'org-structure-template-alist '("tm" . "src elisp :tangle domacs-modules/"))
  (setq org-agenda-files '(
			   "~/Daten/03_Documents/02_Notes/"
			   "~/Daten/03_Documents/02_Notes/02_Projects"
			   ))

  :custom-face
  (org-document-title ((t (:family ,dom/font-slab :height 400))))
  :bind
  ("C-c n c" . org-capture)
  ("C-c o a" . org-agenda)
  )

(use-package org-roam
  :config
  (cond ((memq system-type '(gnu/linux)) (setq org-roam-directory "~/Daten/03_Documents/02_Notes/01_Roam"))
	((memq system-type '(windows-nt)) (setq org-roam-directory "d:/03_Documents/02_Notes/01_Roam"))
	)
  (org-roam-db-autosync-enable)
  )

(use-package consult-org-roam
  :delight
  :ensure t
  :after org-roam
  :init
  (require 'consult-org-roam)
  ;; Activate the minor mode
  (consult-org-roam-mode 1)
  :custom
  ;; Use `ripgrep' for searching with `consult-org-roam-search'
  (consult-org-roam-grep-func #'consult-ripgrep)
  ;; Configure a custom narrow key for `consult-buffer'
  (consult-org-roam-buffer-narrow-key ?r)
  ;; Display org-roam buffers right after non-org-roam buffers
  ;; in consult-buffer (and not down at the bottom)
  (consult-org-roam-buffer-after-buffers t)
  :config
  ;; Eventually suppress previewing for certain functions
  (consult-customize
   consult-org-roam-forward-links
   :preview-key "M-.")
  :bind
  ;; Define some convenient keybindings as an addition
  ("C-c n e" . consult-org-roam-file-find)
  ("C-c n b" . consult-org-roam-backlinks)
  ("C-c n l" . consult-org-roam-forward-links)
  ("C-c n r" . consult-org-roam-search))

(provide 'domacs-org)
