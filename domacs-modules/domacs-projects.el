(use-package project
  :ensure nil
  :custom
  (project-switch-commands 'project-find-file)
  )

(provide 'domacs-projects)
