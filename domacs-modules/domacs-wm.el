(use-package windmove
  :ensure nil
  :bind
  ("C-S-l" . windmove-right)
  ("C-S-j" . windmove-down)
  ("C-S-k" . windmove-up)
  ("C-S-h" . windmove-left)
  ("C-S-M-l" . windmove-swap-states-right)
  ("C-S-M-j" . windmove-swap-states-down)
  ("C-S-M-k" . windmove-swap-states-up)
  ("C-S-M-h" . windmove-swap-states-left)
  )

(use-package window
  :ensure nil
  :config
  (defun split-window-sensibly-prefer-horizontal (&optional window)
    "Based on split-window-sensibly, but designed to prefer a horizontal split,
  i.e. windows tiled side-by-side."
    (let ((window (or window (selected-window))))
      (or (and (window-splittable-p window t)
	       ;; Split window horizontally
	       (with-selected-window window
		 (split-window-right)))
	  (and (window-splittable-p window)
	       ;; Split window vertically
	       (with-selected-window window
		 (split-window-below)))
	  (and
	   (let ((frame (window-frame window)))
	     (or
	      (eq window (frame-root-window frame))
	      (catch 'done
		(walk-window-tree (lambda (w)
				    (unless (or (eq w window)
						(window-dedicated-p w))
				      (throw 'done nil)))
				  frame)
		t)))
	   (not (window-minibuffer-p window))
	   (let ((split-width-threshold 0))
	     (when (window-splittable-p window t)
	       (with-selected-window window
		 (split-window-right))))))))

  (defun split-window-really-sensibly (&optional window)
    (let ((window (or window (selected-window))))
      (if (> (window-total-width window) (* 2 (window-total-height window)))
	  (with-selected-window window (split-window-sensibly-prefer-horizontal window))
	(with-selected-window window (split-window-sensibly window)))))
  :custom
  (split-height-threshold 4)
  (split-width-threshold 40)
  (split-window-preferred-function 'split-window-really-sensibly)
  :bind
  ("C-c w l" . split-window-right)
  ("C-c w j" . split-window-below)
  )

(defun domacs--wm-set-background-color ()
  "A Function to dim the backround color of not-active windows"
  (if (eq (selected-window) (frame-selected-window))
      (custom-set-faces `(default ((t (:background ,(modus-themes-get-color-value 'bg-main))))))
    (custom-set-faces `(default ((t (:background ,(modus-themes-get-color-value 'bg-dim))))))
    )
  )

(add-hook 'window-configuration-change-hook #'domacs--wm-set-background-color)

(use-package ace-window)

(defun domacs--set-window-width (width)
  (if (<= width (window-width))
      (shrink-window-horizontally (- (window-width) width))
    (enlarge-window-horizontally (- width (window-width)))))

(defun domacs--wm-setup-one ()
  "Function to set up a specific Window layout"
  (interactive)
  (let ((customer (read-string "Enter Customer: ")))
    (tab-new)
    (tab-rename customer)
    (find-file (concat "~/ansible-config/environments/" customer "/group_vars/all.yml"))
    (split-window-right)
    (domacs--set-window-width 110)
    (windmove-right)
    (find-file (concat "~/ansible-config/environments/" customer "/inventory"))
    (split-window-right)
    (domacs--set-window-width 70)
    (windmove-right)
    (vterm customer)
    ;; (balance-windows)
    )
  )

(provide 'domacs-wm)
