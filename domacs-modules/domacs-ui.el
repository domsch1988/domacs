(use-package scroll-bar
  :ensure nil
  :config
  (scroll-bar-mode -1)
  )

(use-package spacious-padding
  :custom
  (setq spacious-padding-widths
	'( :internal-border-width 15
	   :header-line-width 4
	   :mode-line-width 6
	   :tab-width 4
	   :right-divider-width 30
	   :scroll-bar-width 8
	   :fringe-width 8))
  (setq spacious-padding-subtle-mode-line
	`( :mode-line-active 'default
	   :mode-line-inactive vertical-border))
  :init
  (spacious-padding-mode 1)
  :bind
  ("C-c u p" . spacious-padding-mode)
  )

(use-package fringe
  :ensure nil
  :config
  (fringe-mode 0)
  )

(use-package dashboard
  :custom
  (dashboard-center-content t)
  (dashboard-vertically-center-content t)
  (dashboard-startup-banner 'logo)
  (dashboard-items '((recents   . 10)
		     (bookmarks . 5)
		     (projects  . 5)
		     (registers . 5)))
  )

(use-package centaur-tabs
  :disabled
  :init
  (centaur-tabs-mode t)
  :custom
  (centaur-tabs-close-button "  ")
  (centaur-tabs-set-modified-marker t)
  (centaur-tabs-modified-marker "  ")
  (centaur-tabs-set-icons t)
  (centaur-tabs-icon-type 'nerd-icons)
  :config
  (defun centaur-tabs-buffer-groups ()
    (list
     (cond
      ((string-equal "*" (substring (buffer-name) 0 1)) "Emacs")
      ((string-equal "*ansi-term*" (substring (buffer-name) 0 5)) "Terminal")
      ((string-equal "*terminal*" (buffer-name)) "Terminal")
      ((string-equal "*vterm*" (buffer-name)) "Terminal")
      (t "GROUP"))))
  :bind
  ("C-c t t" . centaur-tabs-switch-group)
  ("C-h" . centaur-tabs-backward)
  ("C-l" . centaur-tabs-forward))

(use-package tab-line
  :ensure nil
  :after (modus-themes)
  :config
  (defun domacs--customize-tab-line()
    (custom-set-faces
     `(tab-line               ((t (:box (:line-width (1 . 5) :color ,(modus-themes-get-color-value 'bg-tab-bar))))))
     )
    )
  (setq tab-line-close-button " 󰅙  ")
  (setq tab-line-new-button-show nil)
  (defun domacs--tab-line-tab-name-format(tab tabs)
    "Out own function to use as `tab-line-tab-name-format-function'"
    (let* ((buffer-p (bufferp tab))
           (selected-p (if buffer-p
                           (eq tab (window-buffer))
  			 (cdr (assq 'selected tab))))
           (name (if buffer-p
                     (funcall tab-line-tab-name-function tab tabs)
                   (cdr (assq 'name tab))))
           (face (if selected-p
                     (if (mode-line-window-selected-p)
  			 'tab-line-tab-current
                       'tab-line-tab)
                   'tab-line-tab-inactive)))
      (dolist (fn tab-line-tab-face-functions)
  	(setf face (funcall fn tab tabs face buffer-p selected-p)))
      (apply 'propertize
             (concat "  " (propertize (string-replace "%" "%%" name) ;; (bug#57848)
  				      'keymap tab-line-tab-map
  				      'help-echo (if selected-p "Current tab"
                                                   "Click to select tab")
  				      ;; Don't turn mouse-1 into mouse-2 (bug#49247)
  				      'follow-link 'ignore)
                     (or (and (or buffer-p (assq 'buffer tab) (assq 'close tab))
                              tab-line-close-button-show
                              (not (eq tab-line-close-button-show
                                       (if selected-p 'non-selected 'selected)))
                              tab-line-close-button)
  			 ""))
             `(
               tab ,tab
               ,@(if selected-p '(selected t))
               face ,face
               mouse-face tab-line-highlight))))
  (setq tab-line-tab-name-format-function 'domacs--tab-line-tab-name-format)
  :init
  (global-tab-line-mode)
  :hook
  (modus-themes-post-load . domacs--customize-tab-line)
  (tab-line-mode . domacs--customize-tab-line)
  :bind
  ("C-h" . tab-line-switch-to-prev-tab)
  ("C-l" . tab-line-switch-to-next-tab)
  )

(use-package tab-bar
  :ensure nil
  :config
  (setq tab-bar-new-button-show nil)
  (setq tab-bar-close-button " t ")
  (tab-bar-mode -1)
  )

(use-package perfect-margin
  :init
  (perfect-margin-mode 0)
  :custom
  (perfect-margin-visible-width 150)
  :bind
  ("C-c u m" . perfect-margin-mode)
  )

(use-package svg-tag-mode
  :config
  (defun svg-progress-percent (value)
    (save-match-data
      (svg-image (svg-lib-concat
  		  (svg-lib-progress-bar  (/ (string-to-number value) 100.0)
  					 nil :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
  		  (svg-lib-tag (concat value "%")
                               nil :stroke 0 :margin 0)) :ascent 'center)))

  (defun svg-progress-count (value)
    (save-match-data
      (let* ((seq (split-string value "/"))           
             (count (if (stringp (car seq))
  			(float (string-to-number (car seq)))
                      0))
             (total (if (stringp (cadr seq))
  			(float (string-to-number (cadr seq)))
                      1000)))
  	(svg-image (svg-lib-concat
                    (svg-lib-progress-bar (/ count total) nil
                                          :margin 0 :stroke 2 :radius 3 :padding 2 :width 11)
                    (svg-lib-tag value nil
  				 :stroke 0 :margin 0)) :ascent 'center))))
  (setq svg-tag-tags
        '(("Head:" . ((lambda (tag) (svg-tag-make "HEAD" :inverse t :face 'org-done))))
          ;; ("LRe:" . ((lambda (tag) (svg-tag-make "Re"))))
          ;; ("Aw:" . ((lambda (tag) (svg-tag-make "Aw"))))
          ;; ("LAw:" . ((lambda (tag) (svg-tag-make "Aw"))))
          ;; (".[A-Z].*\\]" . ((lambda (tag) (svg-tag-make tag :inverse t :beg 1 :end -1 :face 'magit-keyword))))
          ("\\(ASGUPD-\\)[0-9]+" . ((lambda (tag) (svg-tag-make "ASGUPD" :inverse t :crop-right t :margin 0))))
          ("ASGUPD-\\([0-9]+\\)" . ((lambda (tag) (svg-tag-make tag :inverse t :crop-left t :margin 0 :face 'magit-refname))))
          ("\\(ASGSPT-\\)[0-9]+" . ((lambda (tag) (svg-tag-make "ASGSPT" :inverse t :crop-right t :margin 0 :face 'magit-branch-remote))))
          ("ASGSPT-\\([0-9]+\\)" . ((lambda (tag) (svg-tag-make tag :inverse t :crop-left t :margin 0 :face 'magit-refname))))
          ("Merge:" . ((lambda (tag) (svg-tag-make "MERG" :inverse t :face 'org-todo))))
    	  (":\\([A-Za-z0-9]+\\)" . ((lambda (tag) (svg-tag-make tag))))
          (":\\([A-Za-z0-9]+[ \-]\\)" . ((lambda (tag) tag)))

	  ;; Org TODO State
  	  ("TODO" . ((lambda (tag) (svg-tag-make "TODO" :face 'org-todo :inverse t :margin 0))))
          ("DONE" . ((lambda (tag) (svg-tag-make "DONE" :face 'org-done :margin 0))))

  	  ("\\(\\[[0-9]\\{1,3\\}%\\]\\)" . ((lambda (tag)
                                              (svg-progress-percent (substring tag 1 -2)))))
          ("\\(\\[[0-9]+/[0-9]+\\]\\)" . ((lambda (tag)
                                            (svg-progress-count (substring tag 1 -1)))))
    	  ))
  :hook
  ((magit-mode) . svg-tag-mode)
  (mu4e-headers-mode . svg-tag-mode)
  )

(use-package password-mode
  :hook
  ((
    yaml-mode
    yaml-ts-mode
    ) . password-mode)
  :custom
  (password-mode-password-prefix-regexs
   '("Password:\s+"
     ".*_pw:\s+"
     "passwd:\s+"
     "password_.*:\s+"
     "gpg_pass:\s+"
     "acssharepoint_pw\s+"
     "Passwort:\s+"))
  )

(provide 'domacs-ui)
