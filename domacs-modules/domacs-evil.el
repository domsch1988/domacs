(use-package evil-leader
  :after evil
  :custom
  (evil-want-integration t) ;; This is optional since it's already set to t by default.
  :config
  (evil-leader/set-leader "<SPC>")
  (evil-leader/set-key
    ;; File stuff
    "e" 'project-find-file
    "p" 'project-switch-project
    "sg" 'consult-ripgrep
    "sr" 'recentf
    "bn" 'bookmark-set
    "bm" 'consult-bookmark
    ;; Org
    "ri" 'org-roam-node-find
    "rs" 'consult-org-roam-search
    "of" 'consult-org-heading
    "oj" 'org-insert-heading-after-current
    "os" 'org-insert-subheading
    "ol" 'org-demote-subtree
    "oh" 'org-promote-subtree
    "ot" 'org-insert-todo-heading-respect-content
    "od" 'org-todo
    "nc" 'org-capture
    "ns" 'org-capture-goto-target
    ;; Utility
    "cv" 'eval-defun
    "cs" 'domacs--ssh-to-ip
    ;; Help
    "hv" 'helpful-variable
    "hf" 'helpful-function
    "hc" 'helpful-command
    "ht" 'helpful-at-point
    ;; Buffers
    "<SPC>" 'consult-buffer
    "bk" 'kill-buffer
    "be" 'eval-buffer
    "bi" 'ibuffer
    "cf" 'domacs--indent-buffer
    ;; Window Management
    "wl" 'split-window-right
    "wj" 'split-window-below
    "wq" 'delete-window
    "wr" 'restart-emacs
    "wp" 'password-mode
    "ws" 'spacious-padding-mode
    "wt" 'vterm-other-window
    ;; Git
    "gm" 'magit
    "gl" 'blamer-mode
    "gb" 'vc-region-history
    "t" 'treemacs
    )
  )

(use-package evil
  :config
  (evil-set-initial-state 'eww-mode 'emacs)
  (evil-set-initial-state 'eat-mode 'emacs)
  (evil-set-initial-state 'vterm-mode 'emacs)
  (evil-set-initial-state 'magit-popup-mode 'emacs)
  (evil-set-initial-state 'magit-status-mode 'emacs)
  (evil-set-initial-state 'magit-mode 'emacs)
  (evil-set-initial-state 'dashboard-mode 'emacs)
  (evil-define-key 'normal 'global
    "," 'consult-line
    "gcc" 'evilnc-comment-or-uncomment-lines)
  (evil-define-key 'visual 'global
    "gc" 'evilnc-comment-or-uncomment-lines)
  (setq evil-echo-state nil)
  :init
  (progn
    (setq evil-undo-system 'undo-tree)
    ;; `evil-collection' assumes `evil-want-keybinding' is set to
    ;; `nil' before loading `evil' and `evil-collection'
    ;; @see https://github.com/emacs-evil/evil-collection#installation
    (setq evil-want-keybinding nil)
    )
  (evil-mode 1)
  (global-evil-leader-mode)
  )

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-nerd-commenter
  :after evil
  ;; :config
  ;; (evilnc-default-hotkeys)
  )

(use-package evil-mc
  :disabled
  :init
  (global-evil-mc-mode 1)
  )

(use-package treemacs-evil)

(provide 'domacs-evil)
