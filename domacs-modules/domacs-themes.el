(use-package modus-themes
  :custom
  (modus-themes-custom-auto-reload t)
  (modus-themes-italic-constructs t)
  (modus-themes-bold-constructs t)
  (modus-themes-disable-other-themes t)
  (modus-themes-prompts '(italic bold))
  ( modus-themes-completions
    '((matches . (extrabold))
      (selection . (semibold italic text-also))))
  (modus-themes-headings
   '((1 . (rainbow bold 1.5))
     (2 . (rainbow bold 1.3))
     (2 . (rainbow semibold 1.1))
     (t . (rainbow semibold 1.1))))
  (modus-themes-common-palette-overrides
   '((bg-prose-block-contents bg-dim)
     (bg-prose-block-delimiter bg-inactive)
     (fg-prose-block-delimiter fg-main)))
  (modus-themes-to-toggle '(modus-operandi modus-vivendi-tinted))
  )

(use-package solar
  :ensure nil
  :config
  (setq calendar-latitude 50.79
	calendar-longitude 6.45))

(use-package circadian
  :after solar
  :config
  (setq circadian-themes '((:sunrise . modus-operandi)
			   (:sunset  . modus-vivendi)))
  (circadian-setup))

(provide 'domacs-themes)
