(use-package doom-modeline
  :custom
  (doom-modeline-buffer-file-name-style 'relative-to-project)
  :init
  (doom-modeline-mode 1))

(defvar-local domacs--mode-line-evil-mode
    '(:eval
      (cond
       ((evil-normal-state-p) (propertize " N " 'face 'highlight))
       ((evil-insert-state-p) (propertize " I " 'face 'custom-changed))
       ((evil-emacs-state-p) (propertize " E " 'face 'message-header-to))
       ((evil-visual-state-p) (propertize " V " 'face 'diff-refine-changed))
       )
      )
  )

(defun domacs/mode-line-buffer-name ()
  "Return `buffer-name' with spaces around it."
  (format "%s " (buffer-name)))

(defvar-local domacs--mode-line-buffer-name
    '(:eval
      (if (mode-line-window-selected-p)
	  (propertize (domacs/mode-line-buffer-name) 'face 'bold)
	(propertize (domacs/mode-line-buffer-name) 'face 'shadow)
	)
      )
  )

(defun domacs/mode-line-buffer-path ()
  "Return the path to the file in the current buffer"
  (format "%s" (file-name-directory (buffer-file-name))) 
  )

(defvar-local domacs--mode-line-buffer-path
    '(:eval
      (if (mode-line-window-selected-p)
	  (propertize (domacs/mode-line-buffer-path) 'face 'shadow)
	(propertize (domacs/mode-line-buffer-path) 'face 'shadow)
	)
      )
  )

(defun domacs/mode-line-system-info ()
  "Provide System Information in the mode-line"
  (format "%s" (concat user-login-name "@" system-name))
  )

(defvar-local domacs--mode-line-system-info
    '(:eval
      (propertize (domacs/mode-line-system-info) 'face 'shadow)
      )
  )

(defvar-local domacs--mode-line-spacer
    '(:eval
      (propertize (make-string (- (window-width)
				  (length (format-mode-line mode-line-format)))
			       ?\s) 'face 'shadow)
      )
  )

(defvar-local domacs--header-line-spacer
    '(:eval
      (propertize (make-string (- (window-width)
				  (length (format-mode-line header-line-format)))
			       ?\s) 'face 'shadow)
      )
  )

(defun domacs/mode-line-open-buffers ()
  (let ((buffer-list (buffer-list)))
    (length (cl-remove-if (lambda (buffer)
			    (string-match-p "^ " (buffer-name buffer)))
			  buffer-list)))
  )

(defvar-local domacs--mode-line-open-buffers
    '(:eval
      (propertize (format " %s" (domacs/mode-line-open-buffers)) 'face 'bold)
      )
  )

(dolist (var '(domacs--mode-line-buffer-path
	       domacs--mode-line-buffer-name
	       domacs--mode-line-evil-mode
	       domacs--mode-line-system-info
	       domacs--mode-line-spacer
	       domacs--header-line-spacer
	       domacs--mode-line-open-buffers
	       ))
  (put var 'risky-local-variable t)
  )

;; (setq-default mode-line-format
;; 	      '("%e"
;; 		domacs--mode-line-evil-mode
;; 		domacs--mode-line-spacer
;; 		domacs--mode-line-system-info
;; 		" "
;; 		domacs--mode-line-open-buffers
;; 		" "
;; 		)
;; 	      )

;; (setq-default header-line-format
;; 	      '(
;; 		""
;; 		domacs--mode-line-buffer-path
;; 		domacs--mode-line-buffer-name
;; 		domacs--header-line-spacer
;; 		)
;; 	      )

(use-package time
  :ensure nil
  :config
  (display-time-mode -1)
  :custom
  (display-time-24hr-format t)
  )

(use-package diminish)

(use-package battery
  :ensure nil
  :config
  (display-battery-mode -1)
  )

(use-package delight)

(use-package minions
  :init
  (minions-mode))

(provide 'domacs-modeline)
