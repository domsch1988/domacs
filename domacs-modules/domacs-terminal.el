(defun domacs--vterm (command)
  "Our Custom function to start vterm with an command"
  (interactive)
  (vterm command)
  (vterm-send-string command t)
  (vterm-send-return)
  )

(use-package vterm
  :if (memq window-system '(pgtk))
  :config
  :hook
  (vterm-mode . turn-off-evil-mode)
  (vterm-mode . turn-off-evil-mc-mode)
  :custom
  (vterm-kill-buffer-on-exit t)
  (vterm-max-scrollback 10000)
  (vterm-shell "/usr/bin/zsh")
  )

(use-package eat
  :custom
  (eat-kill-buffer-on-exit t)
  )

(provide 'domacs-terminal)
