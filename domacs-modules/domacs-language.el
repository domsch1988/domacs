(use-package yaml-mode
  :mode
  ("\\.yml\\'" . yaml-ts-mode)
  ("\\.[0-9][0-9][0-9]\\'" . yaml-ts-mode)
  ("\\.[0-9][0-9]\\'" . yaml-ts-mode)
  ("\\.[0-9]\\'" . yaml-ts-mode)
  )

(use-package ini-mode
  :mode
  ("inventory\\'" . ini-mode)
  )



(use-package lua-mode)

(use-package text-mode
  :ensure nil
  :mode
  ("authorized_keys" . text-mode)
  )

(use-package logview)

(provide 'domacs-language)
