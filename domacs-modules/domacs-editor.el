(use-package hl-line
  :ensure nil
  :hook
  ((prog-mode org-mode yaml-ts-mode) . hl-line-mode)
  )

(use-package simple
  :ensure nil
  :hook
  ((prog-mode text-mode) . visual-line-mode)
  ((prog-mode text-mode) . auto-fill-mode)
  )

(use-package display-line-numbers
  :ensure nil
  :custom
  (display-line-numbers-type 'relative)
  :hook
  ((prog-mode yaml-ts-mode) . display-line-numbers-mode)
  )

(use-package treesit
  :ensure nil
  :custom
  (treesit-language-source-alist
   '(
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")
     (ini "https://github.com/justinmk/tree-sitter-ini")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     )
   )
  )

(use-package undo-tree
  :delight
  :custom
  (undo-tree-history-directory-alist '(("." . "~/.config/emacs/.cache")))
  :config (global-undo-tree-mode))

(use-package rainbow-delimiters :init (rainbow-delimiters-mode))

(use-package password-generator :custom (password-generator-simple-length 30))

(use-package ligature
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  (ligature-set-ligatures 'org-mode '("->"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" "::" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "//" "__" "~~" "(*" "*)"
                                       "\\\\" "://"))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

(provide 'domacs-editor)
