(use-package eglot
  :ensure nil
  :config
  (add-to-list 'eglot-server-programs
	       '(yaml-ts-mode . ("ansible-language-server" "--stdio")))
  :hook
  ((yaml-ts-mode) . eglot-ensure)
  )

;; (use-package consult-eglot)

(use-package lsp-mode
  :disabled
  :custom
  (lsp-keymap-prefix "C-c l")
  (lsp-yaml-server-command '("ansible-language-server" "--stdio"))
  ;; (lsp-yaml-validate nil)
  ;; (lsp-yaml-format-enable nil)
  (lsp-ansible-validation-lint-enabled nil)
  (lsp-file-watch-threshold 9999)
  :hook
  (yaml-ts-mode . lsp-deferred)
  (yaml-mode . lsp-deferred)
  :commands lsp)

(use-package flycheck)
(use-package  lsp-treemacs
  :commands lsp-treemacs-errors-list)
(use-package lsp-ui
  :commands lsp-ui-mode)

(provide 'domacs-lsp)
