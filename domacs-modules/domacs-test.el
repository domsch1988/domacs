(require 'shannon-max)
(setq shannon-max-jar-file
      (expand-file-name "~/.config/emacs/domacs-modules/emacskeys-0.1.0-SNAPSHOT-standalone.jar"))
(shannon-max-start-logger)

(use-package fontaine
  :config
  (setq fontaine-latest-state-file
    	(locate-user-emacs-file "fontaine-latest-state.eld"))
  (setq fontaine-presets
      	'((regular
      	   :default-height 100)
      	  (modern
  	           :default-family "JetBrainsMono Nerd Font"
      	   :default-height 100)
      	  (large
      	   :default-height 150)
      	  (present
      	   :default-height 200)
      	  (t
      	   :default-family "FiraCode Nerd Font"
      	   :default-weight normal
      	   :line-spacing 1
      	   ) 
      	  ))
  (fontaine-set-preset (or (fontaine-restore-latest-preset) 'regular))
  (fontaine-mode 1)
  (define-key global-map (kbd "C-c f") #'fontaine-set-preset)
  )

(provide 'domacs-test)
