;; -*- lexical-binding: t; -*-

(defvar elpaca-installer-version 0.7)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
  			      :ref nil
  			      :files (:defaults "elpaca-test.el" (:exclude "extensions"))
  			      :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
  	(if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
  		 ((zerop (call-process "git" nil buffer t "clone"
  				       (plist-get order :repo) repo)))
  		 ((zerop (call-process "git" nil buffer t "checkout"
  				       (or (plist-get order :ref) "--"))))
  		 (emacs (concat invocation-directory invocation-name))
  		 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
  				       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
  		 ((require 'elpaca))
  		 ((elpaca-generate-autoloads "elpaca" repo)))
  	    (progn (message "%s" (buffer-string)) (kill-buffer buffer))
  	  (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

(elpaca elpaca-use-package
  (elpaca-use-package-mode)
  (setq use-package-always-ensure t))
(elpaca-wait)

(defun +elpaca-unload-seq (e)
  (and (featurep 'seq) (unload-feature 'seq t))
  (elpaca--continue-build e))

;; You could embed this code directly in the reicpe, I just abstracted it into a function.
(defun +elpaca-seq-build-steps ()
  (append (butlast (if (file-exists-p (expand-file-name "seq" elpaca-builds-directory))
  		       elpaca--pre-built-steps elpaca-build-steps))
  	  (list '+elpaca-unload-seq 'elpaca--activate-package)))

(use-package seq :ensure `(seq :build ,(+elpaca-seq-build-steps)))

(defvar dom/font-fixed "FiraCode Nerd Font")
(defvar dom/size-fixed 100)
(defvar dom/size-variable 100)
(defvar dom/font-variable)
(defvar dom/font-slab "Iosevka Comfy Wide Motion")
(defvar dom/auto-save-path)
(defvar dom/backup-path)

(setq dom/font-variable
      (cond ((eq system-type 'windows-nt) "Segoe UI")
	    ((eq system-type 'gnu/linux) "Inter Variable")))

;; SSH Connection Shortcut
(defun domacs--ssh-to-ip ()
  "Open ansi-term with an SSH session to the IP address under the cursor."
  (interactive)
  (save-excursion
    (let ((bounds (bounds-of-thing-at-point 'line))
	  (ip-regexp "\\(?:[0-9]*\\.\\)[0-9]*"))
      (when bounds
	(goto-char (car bounds))
	(let ((ip (buffer-substring-no-properties (car bounds) (cdr bounds))))
	  (if (string-match-p ip-regexp ip)
	      (if (fboundp 'domacs--vterm)
		  (domacs--vterm (format "ssh %s" ip))
		(ansi-term (format "ssh %s" ip))
		(message "No valid IP address found under cursor"))))))))

(defun domacs--indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

(mapc
 (lambda (string)
   (add-to-list 'load-path (locate-user-emacs-file string)))
 '("domacs-modules"))

(require 'domacs-themes)

(require 'domacs-basics)

(require 'domacs-projects)

(require 'domacs-modeline)

(require 'domacs-files)

(require 'domacs-evil)

(require 'domacs-keybinds)

(require 'domacs-wm)

(require 'domacs-org)

(require 'domacs-git)

(require 'domacs-completion)

(require 'domacs-language)

(require 'domacs-lsp)

(require 'domacs-editor)

(require 'domacs-help)

(require 'domacs-terminal)

(require 'domacs-ui)

;; (require 'domacs-mail)

(require 'domacs-test)
